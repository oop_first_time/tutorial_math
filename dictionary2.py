countries = {'de': 'Germany', 'ua': 'Ukraine',
             'th': 'Thailand', 'nl': 'Netherlands'}

print(countries.keys()) #dict_keys(['de', 'ua', 'th', 'nl'])
print(countries.values()) #(['Germany', 'Ukraine', 'Thailand', 'Netherlands'])

print(countries.get('de')) #Germany
countries.setdefault('tr', 'Turkey') #

print(countries.popitem())
print(countries.popitem())

print(countries.items())