def after_data(num,numlist):
    for i in range(num):
        if numlist[i] >= 10:
            numlist[i] += 1
        else:
            numlist[i] -= 1
    return numlist
#--------------main-----------------#
num = int(input())
numlist = [0] * num

for i in range(num):
    numlist[i] = int(input())

print('Before data')
print(numlist)
print('After data')
x = after_data(num,numlist)
print(x)
