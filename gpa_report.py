n = int(input())
ids = []
gpas = []

for i in range(n):
    line = input().split(',')
    ids.append( line[0] )
    gpas.append( float(line[1]) )

avg = sum(gpas)/len(gpas)

for i in range(len(ids)):
    print(f'{ids[i]},{abs(gpas[i]-avg):.2f}')